USE northwind2;
select * from
(select OrderID, CustomerID, round(Freight, 1) as fr
	from orders) as ord
    where fr>15;


select * from
(
select OrderID, ShipVia,
case 
	when ShipVia=1 then 'UPS'
    when ShipVia in (2,3) then 'DHL'
    else
	'other'
end descr
	from orders) as ord
where DESCR='DHL';

SELECT
	product, sales
		FROM ( SELECT ProductName Product, SUM(Quantity)Sales 
			FROM products p JOIN order_details od 
            ON p.ProductId = od.ProductId           
GROUP BY ProductName) AS T;