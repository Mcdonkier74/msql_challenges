use trades;

select * from sample_dataset2;
select *, str_to_date(`<date>`,'%Y-%m-%d %H%i') as date_time from sample_dataset2;

SELECT DATE(`<date>`) FROM sample_dataset2;
 /*use this one*/

/*
SELECT `<ticker>`, `<date>`, sum(`<vol>` * `<close>`) / SUM(`<vol>`) AS VWAP
FROM
    sample_dataset2
WHERE
    `<date>` BETWEEN '201010110900' AND '201010111400'
        AND `<ticker>` = 'AAPL'
GROUP BY `<ticker>`
*/
