LOAD DATA INFILE 'C:\sample_dataset2.csv' 
INTO TABLE sample_dataset2 
FIELDS TERMINATED BY ',' 
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;

use trades;

select * from sample_dataset2;

select *, str_to_date(`<date>`,"%Y%m%d %H%i") as date_time from sample_dataset2;


select date_format(`<date>`, "%Y-%m-%d %H%i") from sample_dataset2;

select `<ticker>` from sample_dataset2;
select `<close>` from sample_dataset2;
select sum(`<close>`*`<vol>`)/sum(`<vol>`) as VWAP from sample_dataset2;

SELECT `<ticker>`, `<date>`, sum(`<vol>` * `<close>`) / SUM(`<vol>`) AS VWAP
FROM
    sample_dataset2
WHERE
    `<date>` BETWEEN '201010111200' AND '201010111700'
        AND `<ticker>` = 'AAPL'
GROUP BY `<ticker>`

SELECT DATE(`<date>`) from sample_dataset2;

Delimiter //
create procedure `calculate_volume_weighted_prices`()
begin
	SELECT 
    `<ticker>`, `<date>`, SUM(`<vol>` * `<close>`) / SUM(`<vol>`) AS VWAP
FROM
    sample_dataset2
WHERE
    DATE_FORMAT(str_to_date(`<date>`, "%Y%m%d %H%i")
        AND `<ticker>` = 'AAPL'
GROUP BY `<ticker>`
end;

Delimiter //

call `sample_dataset2`.`calculate_volume_weighted_prices`();