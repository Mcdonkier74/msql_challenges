CREATE DEFINER=`root`@`localhost` PROCEDURE `calculate_volume_weighted_prices`(in start_time varchar(12))
BEGIN
SELECT
    `<ticker>`
    ,DATE_FORMAT(str_to_date(`<date>`, '%Y%m%d %H%i'), '%d-%m-%Y') as`<date>`
    ,DATE_FORMAT(str_to_date(`<date>`, '%Y%m%d %H%i'), '%H-%i') as start_time
    ,DATE_FORMAT(str_to_date(`<date>` + 500, '%Y%m%d %H%i'), '%H-%i') as end_time 
    ,SUM(`<vol>` * `<close>`) / SUM(`<vol>`) AS VWAP
FROM
    sample_dataset2
WHERE
	`<date>` between start_time and start_time + 500
        AND `<ticker>` = 'AAPL'
GROUP BY `<ticker>`;
END