CREATE DEFINER=`root`@`localhost` PROCEDURE `daily_trading_ranges`()
BEGIN
	DROP temporary table if exists temp;
	create temporary table temp as
    SELECT 
    `Date`, MAX(ROUND(ABS(`Open` - `Close`), 4)) AS `Range`
	FROM
		sample_dataset3
	GROUP BY `date`	ORDER BY `Range` DESC LIMIT 3;
    -- SELECT DATE_FORMAT(str_to_date(`Date`, '%m/%d/%Y'), '%d/%m/%Y') as `Date`
	-- 	,`Open`
     --       ,`Close` from sample_dataset3;
    
    DROP temporary table if exists temp1;
	create temporary table temp1 as
    select DATE_FORMAT(str_to_date(`Date`, '%m/%d/%Y'), '%d/%m/%Y') as `Date` 
			,MAX(`High`) as high_price
            from sample_dataset3
            where `Date` in (select `Date` from temp)
			Group by `Date` ORDER BY `High_price` desc;
	-- select * from temp1;
    
	DROP temporary table if exists temp2;
	create temporary table temp2 as
	Select DATE_FORMAT(str_to_date(sample_dataset3.Date, '%m/%d/%Y'), '%d/%m/%Y') as `Date`
		   ,`Time` as `Time`
			From sample_dataset3
            inner join temp1 on DATE_FORMAT(str_to_date(sample_dataset3.Date, '%m/%d/%Y'), '%d/%m/%Y') = temp1.Date
            where sample_dataset3.High = temp1.high_price
			Group by sample_dataset3.Date order by `High_price` desc;
            
	select temp.Date, temp.Range, temp2.Time
		from temp
        right outer join temp2
         on temp2.Time
		 order by temp.Range desc;
		
        
    DROP temporary table if exists temp;
    DROP temporary table if exists temp1;
	DROP temporary table if exists temp2;
	
END